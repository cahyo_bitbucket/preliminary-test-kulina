<html>
<head>
	<title>Preliminary Test Kulina</title>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/bootstrap.min.css'); ?>">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/bootstrap.css'); ?>">
</head>
<body>
	<div class="container">
	<h1>Selamat Datang</h1>
	<h2>Preliminary Test Kulina User Review</h2>
	<!-- Button trigger modal -->
	<button onclick="add_user()" id="addUser" data-toggle="modal" data-target=".bs-modal-AddUser" class="btn btn-primary btn-lg">
	  Add User
	</button>

	<table class="table" id="tableUser">
		<tr>
		  <td >No</td>
		  <td >Order</td>
		  <td >Product</td>
		  <td >User</td>
		  <td >Rating</td>
		  <td >Review</td>
		  <td >Created At</td>
		  <td >Updated At</td>
		  <td >Action</td>
		</tr>

		<?php 
		$no = 1;
		foreach ($user as $datauser) {
		?>
		<tr>
		  <td ><?php echo $no; ?>.</td>
		  <td ><?php echo $datauser['order_id']; ?></td>
		  <td ><?php echo $datauser['product_id']; ?></td>
		  <td ><?php echo $datauser['user_id']; ?></td>
		  <td ><?php echo $datauser['rating']; ?></td>
		  <td ><?php echo $datauser['review']; ?></td>
		  <td ><?php echo $datauser['created_at']; ?></td>
		  <td ><?php echo $datauser['updated_at']; ?></td>
		  <td ><a href="#" data-toggle="modal" data-target=".bs-modal-update<?php echo $datauser['id']; ?>">Update</a> | 
		  		<a href="<?php echo base_url('index.php/home/deleteUser/').$datauser['id']; ?>">Delete</a></td>
		</tr>
		<?php
		$no++;
		}
		 ?>
		
	</table>

	

	


	<script type="text/javascript" src="<?php echo base_url('assets/js/jquery.js'); ?>"></script>
	<script type="text/javascript" src="<?php echo base_url('assets/js/bootstrap.js'); ?>"></script>
	<script type="text/javascript" src="<?php echo base_url('assets/js/bootstrap.min.js'); ?>"></script>
	<script type="text/javascript" src="<?php echo base_url('assets/js/modal.js'); ?>"></script>
	<script src="<?php echo base_url('assets/js/jquery.dataTables.min.js')?>"></script>
	<script src="<?php echo base_url('assets/js/dataTables.bootstrap.js')?>"></script>
	<script type="text/javascript">
		$(document).ready(function(){
			$('#tableUser').DataTable();
		});

	</script>

	<div class="modal fade bs-modal-AddUser" id="modal_form"  role="dialog" >
    <div class="modal-dialog modal-md">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="myModalLabel">Tambah Data</h4>
        </div>
          <div class="modal-body">
          	<form action="<?php echo base_url('index.php/home/addUser'); ?>" method="POST" id="formUser">
          		<label for="inputEmail3" class="col-sm-5 control-label">Masukkan Order</label>
          		<input type="text" name="order_id" class="form-control" placeholder="Order">
          		<label for="inputEmail3" class="col-sm-5 control-label">Masukkan Product</label>
	          <input type="text" name="product_id" class="form-control" placeholder="Product_id">
	          <label for="inputEmail3" class="col-sm-5 control-label">Masukkan User</label>
	          <input type="text" name="user_id" class="form-control" placeholder="User_id">
	          <label for="inputEmail3" class="col-sm-5 control-label">Masukkan Rating</label>
	          <input type="text" name="rating" class="form-control" placeholder="Rating">
	          <label for="inputEmail3" class="col-sm-5 control-label">Masukkan Review</label>
	          <input type="text" name="review" class="form-control" placeholder="Review">
          </div>
          
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <input type="submit" class="btn btn-primary">
            
        </div>
         </form>
      </div>
    </div>
  </div>

	</div>


<?php 
          			foreach ($user as $userUpdateData) {
          		?>
		<div class="modal fade bs-modal-update<?php echo $userUpdateData['id']; ?>" id="modal_form"  role="dialog" >
    <div class="modal-dialog modal-md">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="myModalLabel">Update Data</h4>
        </div>
          <div class="modal-body">
          	
          	<form action="<?php echo base_url('index.php/home/updateUser/').$userUpdateData['id']; ?>" method="POST" id="formUser">
          		<label for="inputEmail3" class="col-sm-5 control-label">Order</label>
          		<input type="text" name="order_id" class="form-control" value="<?php echo $userUpdateData['order_id']; ?>">
		          <label for="inputEmail3" class="col-sm-5 control-label">Product</label>
		          <input type="text" name="product_id" class="form-control" value="<?php echo $userUpdateData['product_id']; ?>">
		          <label for="inputEmail3" class="col-sm-5 control-label">User</label>
		          <input type="text" name="user_id" class="form-control" value="<?php echo $userUpdateData['user_id']; ?>">
		          <label for="inputEmail3" class="col-sm-5 control-label">Rating</label>
		          <input type="text" name="rating" class="form-control" value="<?php echo $userUpdateData['rating']; ?>">
		          <label for="inputEmail3" class="col-sm-5 control-label">Review</label>
		          <input type="text" name="review" class="form-control" value="<?php echo $userUpdateData['review']; ?>">
          		
          		
          </div>
          
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <input type="submit" class="btn btn-primary">
            
        </div>
         </form>
      </div>
    </div>
  </div>

	</div>
	<?php		
          			}
          		 ?>
</body>
</html>