<?php 
/**
* 
*/
class User_model extends CI_Model
{
	
	public function getAllUser(){
		$data = $this->db->query("SELECT * FROM user_review");
		return $data->result_array();
	}

	public function addUser($data){
		$res = $this->db->insert('user_review', $data);
		return $res;
	}	

	public function getUserUpdate($id){
		$data = $this->db->query("SELECT * FROM user_review WHERE id = $id");
		return $data->result_array();
	}

	public function updateUser($data, $id){
		$res = $this->db->update('user_review', $data, $id);
		return $res;
		echo json_encode(array("status" => TRUE));
	}

	public function deleteUser($id){
		$res = $this->db->delete('user_review', $id);
		return $res;
	}
}
 ?>