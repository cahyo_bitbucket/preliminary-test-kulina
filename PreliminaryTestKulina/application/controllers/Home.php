<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

/**
* 
*/
class Home extends CI_Controller
{
	function __construct(){
		parent::__construct();
		$this->load->model('User_model');
	}
	public function index(){
		$data['user'] = $this->User_model->getAllUser();
		$this->load->view('user.php', $data);
	}

	public function addUser(){
		$data = array(
			'id' => null,
			'order_id' => $this->input->post('order_id'),
			'product_id' => $this->input->post('product_id'),
			'user_id' => $this->input->post('user_id'),
			'rating' => $this->input->post('rating'),
			'review' => $this->input->post('review'),
			'created_at' => date("y-m-d"),
			'updated_at' => "");

		$this->User_model->addUser($data);
		echo json_encode(array("status" => TRUE));	
		redirect('home');
	}

	public function getUserUpdate($id){
		$data['userUpdate'] = $this->User_model->getUserUpdate($id);
		$this->load->view('user.php', $data);
	}

	public function updateUser($id){
		$data = array(
			'id' => $id,
			'order_id' => $this->input->post('order_id'),
			'product_id' => $this->input->post('product_id'),
			'user_id' => $this->input->post('user_id'),
			'rating' => $this->input->post('rating'),
			'review' => $this->input->post('review'),
			'created_at' => null,
			'updated_at' => date('y-m-d'));

		$this->User_model->updateUser($data, array('id' => $id));
			
		redirect('home');
	}

	public function deleteUser($id){
		$this->User_model->deleteUser(array('id' => $id));
		redirect('home');
	}
}
 ?>